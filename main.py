
from bs4 import BeautifulSoup
import requests
import datetime

ct = datetime.datetime.now()
ts = ct.timestamp()


f_name=str(ts)+'  1730935.txt'

def job_spider(max_pages):
    page = 1
    while page <= max_pages:
        url = 'https://stackoverflow.com/jobs?pg=' + str(page)
        source_code = requests.get(url)
        plain_text = source_code.text
        soup = BeautifulSoup(plain_text,"html.parser")


        for index,job in enumerate(soup.findAll('a', {'class':'s-link stretched-link'})):
               href = "https://stackoverflow.com" + job.get('href')
               #job_title = job.find('a', class_='s-link stretched-link').text
               title = job.string


               #print(href)
               #print(title)
               with open(f'posts{index}.' + f_name, 'w') as f:
                   f.write(title + '\n')
                   f.write(href + '\n')

        companys = soup.find_all('h3', class_='fc-black-700 fs-body1 mb4')

        for index, company in enumerate(companys):
            com_name = company.find('span').text.replace(' ', '')
            #print(com_name)

            com_country = company.find('span', class_="fc-black-500").text.replace(' ', '')
            #print(com_country)

            with open(f'posts{index}.' + f_name, 'a') as f:
                f.write(com_name + '\n')
                f.write(com_country)

        page += 1


job_spider(1)




